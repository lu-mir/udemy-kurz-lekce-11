import classes from "./Cart.module.css";
import Modal from "./Modal";
import { useContext } from "react";
import AuthContext from "../../store/auth-context";

const Cart = () => {
  const contextProp = useContext(AuthContext);
  const totalAmount = contextProp.totalAmount.toFixed(2);
  const array = contextProp.items;

  // const ids = contextProp.items.map((o) => o.id);
  // console.log(ids)
  // const sortedArray = contextProp.items.filter(
  //   ({ id }, index) => {
  //     return !ids.includes(id, index + 1);
  //   }
  // );

  // const sortedArray = contextProp.items.filter(
  //   (v, i, a) =>
  //     a.findIndex((t) => (t.id === v.id)) === i
  // );

  const increaseAmountInCart = (item) => {
    contextProp.addItem({...item, amount: 1});

  }

  const decreaseAmountInCart = (id) => {
    contextProp.removeItem(id);
    
  }

  return (
    <Modal>
      <div>
        <ul className={classes["cart-items"]}>
          {array.map((item) => (
            <div key={Math.random()}>
              <li>{item.name}</li>
              <li>{item.amount}</li>
              <li>{item.price}</li>
              <div>
                {console.log(item)}
                <button onClick={increaseAmountInCart.bind(null, item)}>+</button>
                <button onClick={decreaseAmountInCart.bind(null, item.id)}>-</button>
              </div>
            </div>
          ))}
        </ul>

        <div className={classes.total}>
          <span>Total Amount</span>
          <span>{totalAmount}</span>
        </div>
        <div className={classes.actions}>
          <button
            onClick={contextProp.onZmena}
            className={classes["button--alt"]}
          >
            Close
          </button>
          <button className={classes.button}>Order</button>
        </div>
      </div>
    </Modal>
  );
};
export default Cart;
