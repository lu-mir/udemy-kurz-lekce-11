import { Fragment } from "react/cjs/react.production.min";
import classes from "./Modal.module.css";
import { useContext } from "react";
import AuthContext from "../../store/auth-context";

const Backdrop = props => {
    const contextProp = useContext(AuthContext);
    return <div className={classes.backdrop} onClick={contextProp.onZmena}/>;
};

const ModalOverlay = props => {
    return (<div className={classes.modal}>
        <div className={classes.content}>{props.children}</div>
    </div>);
};

const Modal = (props) => {
    
  

  return (
  <Fragment>
      <Backdrop />
      <ModalOverlay>{props.children}</ModalOverlay>
  </Fragment>
  );
}
export default Modal;