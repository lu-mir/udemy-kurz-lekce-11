import classes from "./HeaderCartButton.module.css";
import CartIcon from "./CartIcon";
import React, { useContext } from "react"
import AuthContext from "../../store/auth-context";

function HeaderCartButton() {
  const contextProp = useContext(AuthContext);

  const numberOfCartItems = contextProp.items.reduce((curNumber, item) => {
    return curNumber +item.amount;
  }, 0);

  return (
    <button onClick={contextProp.onZmena} className={classes.button}>
      <span className={classes.icon}>
        <CartIcon />
      </span>
      <span>Your Cart</span>
      <span className={classes.badge}>{numberOfCartItems}</span>
    </button>
  );
}
export default HeaderCartButton;
