import MealItem from "./MealItem.js";
import Card from "../UI/Card.js";
import classes from "./AvailableMeals.module.css";
import MealsSummary from "./MealsSummary";
import { Fragment } from "react/cjs/react.production.min";


const DUMMY_MEALS = [
  {
    id: "m1",
    name: "Sushi",
    description: "Finest fish and veggies",
    price: 22.99,
  },
  {
    id: "m2",
    name: "Schnitzel",
    description: "A german specialty!",
    price: 16.5,
  },
  {
    id: "m3",
    name: "Barbecue Burger",
    description: "American, raw, meaty",
    price: 12.99,
  },
  {
    id: "m4",
    name: "Green Bowl",
    description: "Healthy...and green...",
    price: 18.99,
  },
];

function AvailableMeals() {
  return (
    <Fragment>
      <MealsSummary />
      <section className={classes.meals}>
      <Card>
        <ul >
          {DUMMY_MEALS.map((meal) => (
            <MealItem
              key={meal.id}
              id={meal.id}
              name={meal.name}
              description={meal.description}
              price={meal.price}
            />
          ))}
        </ul>
      </Card>
      </section>
    </Fragment>
  );
}

export default AvailableMeals;
