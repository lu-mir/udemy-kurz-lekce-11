import classes from "./MealItem.module.css";
import MealItemForm from "./MealItemForm";
import { useContext } from "react";
import AuthContext from "../../store/auth-context";

function MealItem(props) {
  const contextProps = useContext(AuthContext);
  const price = `$${props.price.toFixed(2)}`;

  const addToCartHandler = (amount) => {
    contextProps.addItem({
      id: props.id,
      name: props.name,
      amount: amount,
      price: props.price
    });
  };

  return (
    <div className={classes.meal}>
      <div>
        <h3>{props.name}</h3>
        <div className={classes.description}>{props.description}</div>
        <div className={classes.price}>{price}</div>
      </div>
      <div>
        <MealItemForm onAddToCart={addToCartHandler} />
      </div>
    </div>
  );
}

export default MealItem;
