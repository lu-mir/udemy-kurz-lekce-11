import classes from "./MealItemForm.module.css";
import { useRef, useState } from "react";

function MealItemForm(props) {
  const [amountIsValid, setAmountIsValid] = useState(true);
  const amountInputRef = useRef();

  const submitHandler = (event) => {
    event.preventDefault();

    const enteredAmount = amountInputRef.current.value;
    const enteredAmountNumber = +enteredAmount;

    if (
      enteredAmount.trim().length === 0 ||
      enteredAmountNumber < 1 ||
      enteredAmountNumber > 5
    ) {
      setAmountIsValid(false);
      return;
    }
    props.onAddToCart(enteredAmountNumber);
  };

  return (
    <form onSubmit={submitHandler} className={classes.form}>
      <div className={classes.input}>
        <label>Amount</label>
        <input
          ref={amountInputRef}
          id= {props.id}
          type="number"
          min="1"
          step="1"
          max="5"
          defaultValue="1"
        />
      </div>
      <div>
        <button type="submit">+Add</button>
      </div>
      {!amountIsValid && <p>Enter valid amount.</p>}
    </form>
  );
}

export default MealItemForm;
