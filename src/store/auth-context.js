import React from "react";

const AuthContext = React.createContext({
 showModal : false,
 onZmena: () => {},
 items: [],
 totalAmount: 0,
 addItem: (item) => {},
 removeItem: (id) => {}
});

export default AuthContext;